package com.lotosuiteplay.level.util.roulette;

import android.content.Context;
import android.content.SharedPreferences;

import com.lotosuiteplay.level.util.SharedPreferenceConstants;

/**
 * Created by wpinango on 9/11/17.
 */

public class SoundConfig {

    public static void setRouletteSoundStatus(Context context, boolean value){
        SharedPreferences preferences = context.getSharedPreferences(SharedPreferenceConstants.KEY_ROULETTE_SOUND, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putBoolean(SharedPreferenceConstants.MUTE_ROULETTE, value);
        edit.apply();
    }

    public static boolean getRouletteSoundStatus(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SharedPreferenceConstants.KEY_ROULETTE_SOUND, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(SharedPreferenceConstants.MUTE_ROULETTE,true);
    }
}
