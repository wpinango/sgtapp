package com.lotosuiteplay.level.util.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.lotosuiteplay.level.activities.MainActivity;
import com.lotosuiteplay.level.Global;
import com.lotosuiteplay.level.activities.RouletteActivity;
import com.lotosuiteplay.level.common.LoginState;
import com.lotosuiteplay.level.common.Time;
import com.lotosuiteplay.level.fragments.NotificationsFragment;
import com.lotosuiteplay.level.fragments.PaymentFragment;
import com.lotosuiteplay.level.fragments.RechargeFragment;
import com.lotosuiteplay.level.fragments.ResultFragment;
import com.lotosuiteplay.level.models.FragmentPosition;
import com.lotosuiteplay.level.models.GameData;
import com.lotosuiteplay.level.models.MessageType;
import com.lotosuiteplay.level.models.Notification;
import com.lotosuiteplay.level.models.RaffleData;
import com.lotosuiteplay.level.models.ResultData;
import com.lotosuiteplay.level.models.ResultNotification;
import com.lotosuiteplay.level.models.Settings;
import com.lotosuiteplay.level.R;
import com.lotosuiteplay.level.models.SimpleDetail;
import com.lotosuiteplay.level.util.notification.NotificationsConfig;
import com.lotosuiteplay.level.util.SharedPreferenceConstants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by wpinango on 8/31/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static String TAG = "MyFirebaseMsgService";
    private NotificationsConfig notificationsConfig = new NotificationsConfig();
    private NotificationID notificationID = new NotificationID();

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        try {
            Log.d(TAG, "From: " + remoteMessage.getFrom());

            if (remoteMessage.getData().size() > 0) {
                Log.d(TAG, "Message data payload: " + remoteMessage.getData());

                SharedPreferences sharedPreferences = getSharedPreferences(SharedPreferenceConstants.KEY_NOTIFICATION, Context.MODE_PRIVATE);
                String value = sharedPreferences.getString("configNotification", "");

                if (value.equals("")) {
                    setSettings();
                } else {
                    getSettings();
                }

                for (Settings settings : Global.settings) {
                    switch (settings.getTAG()) {
                        case SharedPreferenceConstants.KEY_GENERAL_NOTIFICATION:
                            if (settings.isStatus() && !Global.isNotificationShow && LoginState.isLogin(this)) {
                                sendNotification(remoteMessage);
                            }
                            break;
                        case SharedPreferenceConstants.KEY_NOTIFICATION_SOUND:
                            if (settings.isStatus() && LoginState.isLogin(this)) {
                                makeANotificationSound();
                            }
                            break;
                        case SharedPreferenceConstants.KEY_SHOW_RAFFE:
                            if (settings.isStatus() && Global.activityVisible && remoteMessage.getData().containsKey("result") && LoginState.isLogin(this)) {
                                launchRouletteActivity(remoteMessage.getData().get("result"));
                            }
                            break;
                    }
                }

                saveIncomingNotification(remoteMessage.getData());
                if (Global.isNotificationShow && LoginState.isLogin(this)) {
                    sendNotificationToFragmentsView(new Gson().toJson(remoteMessage.getData()));
                } else if (!Global.isNotificationShow && LoginState.isLogin(this)) {
                    checkNotificationMessageType(remoteMessage.getData());
                }
            }

            if (remoteMessage.getNotification() != null) {
                Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            }
        }catch (Exception e) {
            e.getMessage();
        }
    }

    private void saveIncomingNotification(Map<String,String >data ) {
        try {
            ArrayList<Notification> a = new ArrayList<>(Arrays.asList(new Gson().fromJson(NotificationsFragment.getSavedNotifications(this), Notification[].class)));
            if (data.containsKey("title") && !data.get("title").equals("")) {
                Notification notification = new Notification();
                notification.setTitle(data.get("title"));
                notification.setHour(Time.getNotificationDate(data.get("notificationDate")));
                a.add(0, notification);
            }
            if (data.containsKey("result")) {
                Notification n = new Notification();
                ResultNotification resultNotification = new Gson().fromJson(data.get("result"), ResultNotification.class);
                n.setResult(resultNotification);
                n.setTitle("Ver repeticion de resultado de " + resultNotification.getRaffle() + " en la ruleta");
                n.setHour(Time.getNotificationDate(data.get("notificationDate")));
                a.add(0, n);
            }
            NotificationsFragment.saveNotifications(this, new Gson().toJson(a));
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void checkNotificationMessageType(Map<String, String> data) {
        try {
            if (data.containsKey(MessageType.KEY_MESSAGE)) {
                String dataContains = data.get(MessageType.KEY_MESSAGE);
                if (dataContains.equals(MessageType.KEY_PAYMENT)) {
                    PaymentFragment.refresh = "refresh";
                }
                if (dataContains.equals(MessageType.KEY_RECHARGE)) {
                    RechargeFragment.refresh = "refresh";
                }
                if (dataContains.equals(MessageType.KEY_RESULT)) {
                    ResultFragment.refresh = "refresh";
                }
                updateFragmentViewFromNotification(data.get(MessageType.KEY_MESSAGE));
            }
        } catch (Exception e){
            e.getMessage();
        }
    }

    private void launchRouletteActivity(String value) {
        ResultNotification resultNotifications = new Gson().fromJson(value, ResultNotification.class);
        ArrayList<SimpleDetail> simpleDetails = resultNotifications.getResults();
        GameData g = new GameData();
        GameData g1 = new GameData();
        GameData g2 = new GameData();
        LinkedHashMap<String, GameData> data = new LinkedHashMap<String, GameData>();
        for (SimpleDetail s : simpleDetails) {
            switch (s.getCodeName()) {
                case Global.KEY_SAF:
                    g.chipWinner = s.getChip();
                    data.put(Global.KEY_SAF, g);
                    Global.resultImage[0] = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(s.getChipDescription().trim().toLowerCase(), "drawable", this.getPackageName()));
                    break;
                case Global.KEY_TIZ:
                    g1.chipWinner = s.getChip();
                    data.put(Global.KEY_TIZ, g1);
                    Global.resultImage[1] = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(s.getChipDescription().trim().toLowerCase(), "drawable", this.getPackageName()));
                    if (s.getChipDescription().equals(Global.KEY_PINEAPPLE)) {
                        Global.resultImage[1] = BitmapFactory.decodeResource(getResources(), R.drawable.pina);
                    }
                    break;
                case Global.KEY_LOT:
                    g2.chipWinner = s.getChip();
                    data.put(Global.KEY_LOT, g2);
                    Global.resultImage[2] = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(s.getChipDescription().trim().toLowerCase(), "drawable", this.getPackageName()));
                    break;
            }
        }
        String[] rotation = resultNotifications.getRotationTime().split(",");
        ResultData r = new ResultData();
        r.currentResult = new RaffleData();
        r.currentResult.results = data;
        r.timeRotation = new int[3];
        r.timeRotation[0] = Integer.valueOf(rotation[0]) ;      //Loto
        r.timeRotation[1] = Integer.valueOf(rotation[1]) ;      //Tizana
        r.timeRotation[2] = Integer.valueOf(rotation[2]) ;      //Safari
        r.currentResult.raffleWinSerial = resultNotifications.getSerial();
        r.currentResult.raffleName = resultNotifications.getRaffle();
        Intent intent = new Intent(this, RouletteActivity.class);
        intent.putExtra("roulette", new Gson().toJson(r));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    protected void sendNotificationToFragmentsView(String value) {
        Intent intent = new Intent("com.ruletadigital.ruletaplay.fragments.NotificationsFragment.action.UI_UPDATE");
        intent.putExtra("UI_KEY", value);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void updateFragmentViewFromNotification(String value) {
        Intent intent = new Intent("com.ruletadigital.ruletaplay.fragments.Fragment.action.UI_UPDATE");
        intent.putExtra("UI_KEY2",value);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /**
     * Schedule a job using FirebaseJobDispatcher.
     */
    /*private void scheduleJob() {
        // [START dispatch_job]
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag("my-job-tag")
                .build();
        dispatcher.schedule(myJob);
        // [END dispatch_job]
    }*/

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param remoteMessage FCM message body received.
     */
    private void sendNotification(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().containsKey("result") && !Global.activityVisible) {
            Intent intent = new Intent(this, MainActivity.class);
            ResultNotification resultNotification = new Gson().fromJson(remoteMessage.getData()
                    .get("result"),ResultNotification.class);
            intent.putExtra("roulette", new Gson().toJson(resultNotification));
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.lotoplay)
                    .setContentTitle("LotoSuite Play")
                    .setContentText("Ver repeticion de resultado de " + resultNotification.getRaffle()
                            + " en la ruleta")
                    .setAutoCancel(true)
                    //.setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(1, notificationBuilder.build());
        }
        if (remoteMessage.getData().containsKey("title") && !remoteMessage.getData().get("title").equals("")) {
            Intent intent = new Intent(this, MainActivity.class);
            if (remoteMessage.getData().containsKey(MessageType.KEY_MESSAGE)){
                String value = remoteMessage.getData().get(MessageType.KEY_MESSAGE);
                if (value.equals(MessageType.KEY_PAYMENT)){
                    intent.putExtra(MessageType.KEY_MESSAGE, FragmentPosition.paymentFragment);
                } else if (value.equals(MessageType.KEY_RECHARGE)){
                    intent.putExtra(MessageType.KEY_MESSAGE,FragmentPosition.rechargeFragment);
                } else if (value.equals(MessageType.KEY_SYSTEM_MESSAGE)){
                    intent.putExtra(MessageType.KEY_SYSTEM_MESSAGE,FragmentPosition.notificationFragment);
                }
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.lotoplay)
                    .setContentText(remoteMessage.getData().get("title"))
                    .setContentTitle("LotoSuite Play")
                    .setAutoCancel(true)
                    //.setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(notificationID.getID(), notificationBuilder.build());
        }
    }

    private void makeANotificationSound() {
        try {
            Vibrator vb = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
            vb.vibrate(600);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getSettings() {
        ArrayList<Settings> settings = new ArrayList<>();
        Global.settings.clear();
        Settings[] s = new Gson().fromJson(notificationsConfig.getNotification(this), Settings[].class);
        for (Settings a : s) {
            settings.add(a);
        }
        Global.settings.addAll(settings);
        return new Gson().toJson(settings);
    }

    private void setSettings() {
        ArrayList<Settings> settings = new ArrayList<>();
        Global.settings.clear();
        Settings sound = new Settings();
        sound.setTitle(Global.NAME_NOTIFICATION_SOUND);
        sound.setTAG(SharedPreferenceConstants.KEY_NOTIFICATION_SOUND);
        sound.setStatus(true);
        Settings notification = new Settings();
        notification.setStatus(true);
        notification.setTAG(SharedPreferenceConstants.KEY_GENERAL_NOTIFICATION);
        notification.setTitle(Global.NAME_NOTIFICATION);
        Settings showRaffle = new Settings();
        showRaffle.setStatus(true);
        showRaffle.setTAG(SharedPreferenceConstants.KEY_SHOW_RAFFE);
        showRaffle.setTitle(Global.NAME_RAFFLE);
        settings.add(notification);
        settings.add(sound);
        settings.add(showRaffle);
        notificationsConfig.setNotificationConfig(this, new Gson().toJson(settings));
        Global.settings.addAll(settings);
    }

    private class NotificationID {
        private final AtomicInteger c = new AtomicInteger(0);
        private int getID() {
            return c.incrementAndGet();
        }
    }
}

