package com.lotosuiteplay.level.util.rackMonthPicker.listener;

import com.lotosuiteplay.level.util.rackMonthPicker.MonthRadioButton;

/**
 * Created by kristiawan on 31/12/16.
 */

public interface MonthButtonListener {
    public void monthClick(MonthRadioButton objectMonth);
}
