package com.lotosuiteplay.level.activities;

import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;

import com.google.gson.Gson;
import com.lotosuiteplay.level.adapters.SettingsListAdapter;
import com.lotosuiteplay.level.common.TransitionAnimation;
import com.lotosuiteplay.level.Global;
import com.lotosuiteplay.level.models.Settings;
import com.lotosuiteplay.level.R;
import com.lotosuiteplay.level.util.notification.NotificationsConfig;
import com.lotosuiteplay.level.util.SharedPreferenceConstants;

import java.util.ArrayList;

/**
 * Created by wpinango on 9/5/17.
 */

public class SettingsActivity extends BaseActivity {
    private SettingsListAdapter settingsListAdapter;
    private ArrayList<Settings>settings = new ArrayList<>();
    private NotificationsConfig notificationsConfig = new NotificationsConfig();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_settings);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setSubtitle("Configuracion");
        if (notificationsConfig.getNotification(this).equals("")){
            setSettings();
        } else {
            getSettings();
        }
        ListView listView = (ListView)findViewById(R.id.lv_settings);
        settingsListAdapter = new SettingsListAdapter(this, Global.settings);
        listView.setAdapter(settingsListAdapter);
        /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (settingsListAdapter.getItem(i)){
                    case R.layout.item_list_setting_sound:
                        showSound();
                        break;
                }
            }
        });*/
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

    @Override
    protected int getLayoutResoursesId() {
        return R.layout.activity_settings;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showSjound(){
        Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select Tone");
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, (Uri) null);
        this.startActivityForResult(intent, 5);
    }

    private String getSettings() {
        settings.clear();
        Global.settings.clear();
        Settings[] s = new Gson().fromJson(notificationsConfig.getNotification(this),Settings[].class);
        for (Settings a : s){
            settings.add(a);
        }
        Global.settings.addAll(settings);

        return new Gson().toJson(settings);
    }

    private void setSettings(){
        settings.clear();
        Global.settings.clear();
        Settings sound = new Settings();
        sound.setTitle(Global.NAME_NOTIFICATION_SOUND);
        sound.setTAG(SharedPreferenceConstants.KEY_NOTIFICATION_SOUND);
        sound.setStatus(true);
        Settings notification = new Settings();
        notification.setStatus(true);
        notification.setTAG(SharedPreferenceConstants.KEY_GENERAL_NOTIFICATION);
        notification.setTitle(Global.NAME_NOTIFICATION);
        Settings showRaffle = new Settings();
        showRaffle.setStatus(true);
        showRaffle.setTAG(SharedPreferenceConstants.KEY_SHOW_RAFFE);
        showRaffle.setTitle(Global.NAME_RAFFLE);
        settings.add(notification);
        settings.add(sound);
        settings.add(showRaffle);
        notificationsConfig.setNotificationConfig(this,new Gson().toJson(settings));
        Global.settings.addAll(settings);
    }
}
