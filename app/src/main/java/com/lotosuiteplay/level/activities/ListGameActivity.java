package com.lotosuiteplay.level.activities;

import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.lotosuiteplay.level.adapters.GameListAdapters.GameListAdapter;
import com.lotosuiteplay.level.common.TransitionAnimation;
import com.lotosuiteplay.level.Global;
import com.lotosuiteplay.level.R;
import com.lotosuiteplay.level.adapters.GamesChipGridAdapter;

import java.util.ArrayList;
import java.util.Arrays;

public class ListGameActivity extends BaseActivity {

    private SwipeMenuListView gameList;
    private GamesChipGridAdapter Adapter;
    private AlertDialog iconDialog;
    private String[] animalName, fruitName, lottoPlay;
    private GameListAdapter listAdapter2;
    private ArrayList<String> lotoName = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_list_game);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setSubtitle("Juegos disponible");
        lotoName = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.LottoNames)));
        listAdapter2 = new GameListAdapter(this,lotoName,Global.lotoLogo);
        gameList = (SwipeMenuListView)findViewById(R.id.gameList);
        /*SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item backgrounda
                openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9,
                        0xCE)));
                // set item width
                openItem.setWidth(dp2px(90));
                // set item title
                openItem.setTitle("Open");
                // set item title fontsize
                openItem.setTitleSize(18);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item backgrounda
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(dp2px(90));
                // set a icon
                deleteItem.setIcon(R.drawable.trash_white);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        gameList.setMenuCreator(creator);

        gameList.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // open
                        break;
                    case 1:
                        // delete
                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });*/
        gameList.setAdapter(listAdapter2);
        gameList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MosAlerta(position);
            }
        });
        animalName = getResources().getStringArray(R.array.animal_names);
        fruitName = getResources().getStringArray(R.array.fruit_name);
        lottoPlay = getResources().getStringArray(R.array.lottery_name);
    }

    @Override
    protected int getLayoutResoursesId() {
        return R.layout.activity_list_game;
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void MosAlerta(int position) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View ViewAlerta = inflater.inflate(R.layout.grid_view_general, null);
        GridView xGrilla = (GridView) ViewAlerta.findViewById(R.id.grdGrilla);
        TextView xTitulo = (TextView) ViewAlerta.findViewById(R.id.lblTitu);

        switch (position){
            case 0:
                Adapter = new GamesChipGridAdapter(this, animalName, Global.newSafariImages);
                xTitulo.setText("Safari Play");
                break;
            case 1:
                Adapter = new GamesChipGridAdapter(this, fruitName, Global.newTizanaImages);
                xTitulo.setText("Tizana Play");
                break;
            case 2:
                Adapter = new GamesChipGridAdapter(this, lottoPlay, Global.newLotoImages);
                xTitulo.setText("Loto Play");
                break;
        }

        xGrilla.setAdapter(Adapter);
        builder.setNegativeButton("Cerrar", null);
        builder.setView(ViewAlerta);
        iconDialog = builder.create();
        iconDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

}
