package com.lotosuiteplay.level.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.lotosuiteplay.level.activities.login.LoginActivity;
import com.lotosuiteplay.level.activities.login.UpdatePasswordActivity;
import com.lotosuiteplay.level.adapters.SectionsPagerAdapter;
import com.lotosuiteplay.level.common.LoginState;
import com.lotosuiteplay.level.common.Time;
import com.lotosuiteplay.level.common.TransitionAnimation;
import com.lotosuiteplay.level.Global;
import com.lotosuiteplay.level.R;
import com.lotosuiteplay.level.models.FragmentPosition;
import com.lotosuiteplay.level.models.GameData;
import com.lotosuiteplay.level.models.MessageType;
import com.lotosuiteplay.level.models.RaffleData;
import com.lotosuiteplay.level.models.ResultData;
import com.lotosuiteplay.level.models.ResultNotification;
import com.lotosuiteplay.level.models.SimpleDetail;
import com.lotosuiteplay.level.sqlite.DBHelper;
import com.lotosuiteplay.level.util.appRaterLib.AppRater;
import com.lotosuiteplay.level.util.timeout.TimerMethod;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private TabLayout tabLayout;
    private FragmentManager mFragmentManager;
    private final String ACTION_INTENT2 = "com.ruletadigital.ruletaplay.fragments.NotificationsFragment.action.UI_UPDATE";
    private final String ACTION_INTENT = "com.ruletadigital.ruletaplay.fragments.Fragment.action.UI_UPDATE";
    private TimerMethod timerMethod = new TimerMethod();
    private Handler handler = new Handler();
    private Runnable runnable;
    private long milliSeconds = 0;
    private TextView tvNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.app_name));
        toolbar.setPopupTheme(R.style.PopupTheme);
        setSupportActionBar(toolbar);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOnPageChangeListener(myOnPageChangeListener);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        mFragmentManager = getSupportFragmentManager();
        String version = "";
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        TextView tvVersion = (TextView)headerView.findViewById(R.id.tv_version);
        tvVersion.setText("version " + version);
        if (showAnimationLabel()) {
            tvNew = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_help_media));
            initializeCountDrawer();
            setTexViewAnimation(tvNew);
        }
        FirebaseMessaging.getInstance().subscribeToTopic("news");
        //showDialog();
        AppRater.setPackageName(this.getPackageName());
        AppRater.app_launched(this);
        Global.refreshedToken = FirebaseInstanceId.getInstance().getToken();
        IntentFilter filter = new IntentFilter(ACTION_INTENT);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
        IntentFilter filterNotifications = new IntentFilter(ACTION_INTENT2);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiverNotification, filterNotifications);
        try {
            int value = getIntent().getIntExtra(MessageType.KEY_MESSAGE, FragmentPosition.notificationFragment);
            String value2 = getIntent().getStringExtra("roulette");
            if (value == FragmentPosition.paymentFragment) {
                mViewPager.setCurrentItem(FragmentPosition.paymentFragment);
                mSectionsPagerAdapter.requestPendingPayments();
            } else if (value == FragmentPosition.rechargeFragment) {
                mViewPager.setCurrentItem(FragmentPosition.rechargeFragment);
                mSectionsPagerAdapter.requestPendingRecharge();
            } /*else if (value == FragmentPosition.notificationFragment) {
                mViewPager.setCurrentItem(FragmentPosition.notificationFragment);
            }*/
            if (value2 != null) {
                launchRouletteActivity(value2);
                mViewPager.setCurrentItem(FragmentPosition.resultFragment);
                mSectionsPagerAdapter.requestResultList();
            }
            getIntent().removeExtra("key");
        } catch (Exception e) {
            e.getMessage();
        }
        //countDownStart();
    }

    private void setTexViewAnimation(TextView textView){
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(400); //You can manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        textView.setAnimation(anim);
    }

    private void initializeCountDrawer(){
        tvNew.setGravity(Gravity.CENTER_VERTICAL);
        tvNew.setTypeface(null, Typeface.BOLD);
        tvNew.setTextColor(getResources().getColor(R.color.colorAccent));
        tvNew.setText("Nuevo");
    }

    public void refreshPaymentFragment() {
        mSectionsPagerAdapter.requestPendingPayments();
    }

    @Override
    protected int getLayoutResoursesId() {
        return R.layout.activity_main;
    }

    /*private void showDialog() {
        if (isShowBetaVersion()) {
            showBetaVersion();
        }
    }*/

   /* private void showBetaVersion() {
        new SweetAlertDialog(this, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText("Informacion")
                .setContentText("Esta es una version beta, por favor reporte los errores a este correo info@lotosuite.com")
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                })
                .setCancelText("No mostrar de nuevo")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        SharedPreferences preferences = getSharedPreferences("Beta", Context.MODE_PRIVATE);
                        SharedPreferences.Editor edit = preferences.edit();
                        edit.putBoolean("isBeta", false);
                        edit.apply();
                        sweetAlertDialog.cancel();
                    }
                })
                .show();
    }*/

    private boolean showAnimationLabel() {
        SharedPreferences sharedPreferences = getSharedPreferences("Label", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("isShow", true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Global.selectURL();
        try {
            /*if (Global.lastActivityTime == 0){
                Global.lastActivityTime = TimerMethod.getLastActivityTime(this);
            }
            long now = Time.getCurrentTimeInSeconds();
            if ((now - Global.lastActivityTime) > TimerMethod.time && Global.lastActivityTime != 0){
                LoginState.setLoginState(this, false);
                Toast.makeText(this,"Expiro la sesion",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
            }*/ // TODO descomentar este bloque
            if (Global.token.equals("")) {
                DBHelper dbHelper = new DBHelper(this);
                Cursor res = dbHelper.getToken();
                if (res.moveToFirst()) {
                    Global.token = res.getString(res.getColumnIndex("token"));
                    Global.userId = res.getString(res.getColumnIndex("userid"));
                    Global.agencyId = res.getString(res.getColumnIndex("agency"));
                    Global.refreshedToken = FirebaseInstanceId.getInstance().getToken();
                }
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Global.lastActivityTime = Time.getCurrentTimeInSeconds();
        return super.dispatchTouchEvent(ev);
    }

    ViewPager.OnPageChangeListener myOnPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrollStateChanged(int state) {
            //Called when the scroll state changes.
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            hideKeyboard();
        }

        @Override
        public void onPageSelected(int position) {
            hideKeyboard();
        }
    };

    public void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        // check if no view has focus:
        View v = getCurrentFocus();
        if (v == null)
            return;
        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        //TimerMethod.setLastActivityTime(this, Time.getCurrentTimeInSeconds()); // TODO descomentar esta linea
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            return true;
        } else if ( id == R.id.action_close_session) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setTitle("Importante")
                    .setMessage("Desea cerrar la sesion?")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            LoginState.setLoginState(MainActivity.this, false);
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(intent);
                            new CloseToken().execute(Global.URL_CLOSE_TOKEN);
                            finish();
                        }
                    });
            builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            builder.show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_user) {
            Intent intent = new Intent(this, UserActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else if (id == R.id.nav_roulett) {
            Intent intent = new Intent(this, ListGameActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else if (id == R.id.sing_out) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setTitle("Importante")
                    .setMessage("Desea cerrar la sesion?")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            LoginState.setLoginState(MainActivity.this, false);
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(intent);
                            TransitionAnimation.setOutActivityTransition(MainActivity.this);
                            new CloseToken().execute(Global.URL_CLOSE_TOKEN);
                            finish();
                        }
                    });
            builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            builder.show();
        } else if (id == R.id.nav_share) {
            final String appPackageName = this.getPackageName();
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name) + "\n\n");
            String shareApp = "\nDescarga " + getResources().getString(R.string.app_name) + " y juega online\n\n";
            String link = "https://play.google.com/store/apps/details?id=" + appPackageName;
            sharingIntent.putExtra(Intent.EXTRA_TEXT, shareApp + " " + link);
            startActivity(Intent.createChooser(sharingIntent, "Seleccione uno"));
        } else if (id == R.id.nav_rate_producer) {
            Intent intent = new Intent(this, RateProducerActivity.class);
            startActivity(intent);
            TransitionAnimation.setInActivityTransition(this);
        } else if (id == R.id.nav_change_producer) {
            Intent intent = new Intent(this, ChangeProducerActivity.class);
            startActivity(intent);
            TransitionAnimation.setInActivityTransition(this);
        } else if (id == R.id.nav_trans_money) {
            Intent intent = new Intent(this, TransferCashActivity.class);
            startActivity(intent);
            TransitionAnimation.setInActivityTransition(this);
        } else if (id == R.id.nav_information_producer) {
            Intent intent = new Intent(this, ProducerInformationActivity.class);
            startActivity(intent);
            TransitionAnimation.setInActivityTransition(this);
        } /*else if (id == R.id.nav_chat) {
            Intent intent = new Intent(this, ChatActivity.class );
            startActivity(intent);
            TransitionAnimation.setInActivityTransition(this);
        }*/ else if (id == R.id.nav_bank_account) {
            Intent intent = new Intent(this, BankAccountActivity.class);
            startActivity(intent);
            TransitionAnimation.setInActivityTransition(this);
        } else if (id == R.id.nav_change_password) {
            Intent intent = new Intent(this, UpdatePasswordActivity.class);
            startActivity(intent);
            TransitionAnimation.setInActivityTransition(this);
        } else if (id == R.id.nav_rate_app) {
            AppRater.showRateDialog(this);
        } else if (id == R.id.nav_help_media){
            Intent intent = new Intent(this, TutorialActivity.class);
            startActivity(intent);
            TransitionAnimation.setInActivityTransition(this);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private class CloseToken extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA, Global.KEY_MOVIL)
                        .contentType("application/json")
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(MainActivity.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
        }
    }

    public void setCash() {
        mSectionsPagerAdapter.setCash();
    }

    public void updateBalanceAmounts() {
        mSectionsPagerAdapter.updateBalanceAmount();
    }

    public void setNewRechargeCash() {
        mSectionsPagerAdapter.updateRechargeCash();
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_INTENT.equals(intent.getAction()) && Global.activityVisible) {
                String value = intent.getStringExtra("UI_KEY2");
                updateCurrentFragmentFromNotification(value);
            }
        }
    };

    protected BroadcastReceiver receiverNotification = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_INTENT2.equals(intent.getAction())) {
                String value = intent.getStringExtra("UI_KEY");
                setNotificationReceive(value);
            }
        }
    };

    private void setNotificationReceive(String value) {
        mSectionsPagerAdapter.updateNotificationReceive(value);
    }

    private void updateCurrentFragmentFromNotification(String value) {

        if (value.equals(MessageType.KEY_RECHARGE) && mViewPager.getCurrentItem() == FragmentPosition.rechargeFragment) {
            mSectionsPagerAdapter.requestPendingRecharge();
            mSectionsPagerAdapter.refreshBalance();
            mSectionsPagerAdapter.requestPendingPayments();
        } else if (value.equals(MessageType.KEY_PAYMENT) && mViewPager.getCurrentItem() == FragmentPosition.paymentFragment) {
            mSectionsPagerAdapter.requestPendingPayments();
        } else if (value.equals(MessageType.KEY_RESULT) && mViewPager.getCurrentItem() == FragmentPosition.resultFragment) {
            mSectionsPagerAdapter.requestResultList();
        } else if (value.equals(MessageType.KEY_PAYMENT) && mViewPager.getCurrentItem() == FragmentPosition.balanceFragment) {
            mSectionsPagerAdapter.requestPendingRecharge();
            mSectionsPagerAdapter.refreshBalance();
        } else if (value.equals(MessageType.KEY_RECHARGE) && mViewPager.getCurrentItem() == FragmentPosition.balanceFragment) {
            mSectionsPagerAdapter.requestPendingRecharge();
            mSectionsPagerAdapter.refreshBalance();
        } else if (value.equals(MessageType.KEY_RESULT) && mViewPager.getCurrentItem() == FragmentPosition.ticketFragment) {
            mSectionsPagerAdapter.refreshTickets();
        } else if (value.equals(MessageType.KEY_RESULT) && mViewPager.getCurrentItem() == FragmentPosition.primaryFragment) {
            mSectionsPagerAdapter.refreshRafflesAndChips();
        } else if (value.equals(MessageType.KEY_RECHARGE) && mViewPager.getCurrentItem() == FragmentPosition.primaryFragment) {
            mSectionsPagerAdapter.refreshRafflesAndChips();
        }
    }

    private void launchRouletteActivity(String value) {
        ResultNotification resultNotifications = new Gson().fromJson(value, ResultNotification.class);
        ArrayList<SimpleDetail> simpleDetails = resultNotifications.getResults();
        GameData g = new GameData();
        GameData g1 = new GameData();
        GameData g2 = new GameData();
        LinkedHashMap<String, GameData> data = new LinkedHashMap<>();
        for (SimpleDetail s : simpleDetails) {
            switch (s.getCodeName()) {
                case Global.KEY_SAF:
                    g.chipWinner = s.getChip();
                    data.put(Global.KEY_SAF, g);
                    Global.resultImage[0] = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(s.getChipDescription().trim().toLowerCase(), "drawable", this.getPackageName()));
                    break;
                case Global.KEY_TIZ:
                    g1.chipWinner = s.getChip();
                    data.put(Global.KEY_TIZ, g1);
                    Global.resultImage[1] = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(s.getChipDescription().trim().toLowerCase(), "drawable", this.getPackageName()));
                    if (s.getChipDescription().equals(Global.KEY_PINEAPPLE)) {
                        Global.resultImage[1] = BitmapFactory.decodeResource(getResources(), R.drawable.pina);
                    }
                    break;
                case Global.KEY_LOT:
                    g2.chipWinner = s.getChip();
                    data.put(Global.KEY_LOT, g2);
                    Global.resultImage[2] = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(s.getChipDescription().trim().toLowerCase(), "drawable", this.getPackageName()));
                    break;
            }
        }
        String[] rotation = resultNotifications.getRotationTime().split(",");
        ResultData r = new ResultData();
        r.currentResult = new RaffleData();
        r.currentResult.results = data;
        r.timeRotation = new int[3];
        r.timeRotation[0] = Integer.valueOf(rotation[0]);      //Loto
        r.timeRotation[1] = Integer.valueOf(rotation[1]);      //Tizana
        r.timeRotation[2] = Integer.valueOf(rotation[2]);      //Safari
        r.currentResult.raffleWinSerial = resultNotifications.getSerial();
        r.currentResult.raffleName = resultNotifications.getRaffle();
        Intent intent = new Intent(this, RouletteActivity.class);
        intent.putExtra("roulette", new Gson().toJson(r));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void setToolbarTime(String value) {
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.app_name) + value);
    }

    public void countDownStart() {
        milliSeconds = (Global.rafflesByGames.get(0).getHour() - Time.getCurrentTime()) * 60 * 1000;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm:ss");
        SimpleDateFormat minuteFormat = new SimpleDateFormat("mm");
        SimpleDateFormat secondFormat = new SimpleDateFormat("ss");
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    int status = timerMethod.checkTimeForNextRaffle(Global.rafflesByGames.get(0).getHour(), Time.getCurrentTime());
                    if (status == 2) {
                        int minute = Integer.valueOf(minuteFormat.format(System.currentTimeMillis()));
                        int seconds = Integer.valueOf(secondFormat.format(System.currentTimeMillis()));
                        if (minute > 30) {
                            minute = minute - 60;
                        } else {
                            minute = minute - 30;
                        }
                        seconds = seconds - 60;
                        setToolbarTime("   -  [ " + String.valueOf(minute) + ":" + seconds + " ]");
                        System.out.println();
                            /*setToolbarTime("   -  [ " + String.format("%02d", minutes)
                                    + ":" + String.format("%02d", seconds) + " ]");*/
                    } else {
                        setToolbarTime("");
                    }
                    //System.out.println("valores " + Global.rafflesByGames.get(0).getHour() + " " + Time.getCurrentTime() + " " + status);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        }).start();
    }
}
