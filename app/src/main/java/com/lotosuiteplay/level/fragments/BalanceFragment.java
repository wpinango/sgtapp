package com.lotosuiteplay.level.fragments;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.lotosuiteplay.level.activities.MainActivity;
import com.lotosuiteplay.level.adapters.BalanceListAdapter;
import com.lotosuiteplay.level.common.Format;
import com.lotosuiteplay.level.common.Time;
import com.lotosuiteplay.level.Global;
import com.lotosuiteplay.level.models.Balance;
import com.lotosuiteplay.level.models.MyBalance;
import com.lotosuiteplay.level.models.Response;
import com.lotosuiteplay.level.R;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.lotosuiteplay.level.util.SharedPreferenceConstants;
import com.lotosuiteplay.level.util.rackMonthPicker.RackMonthPicker;
import com.lotosuiteplay.level.util.rackMonthPicker.listener.DateMonthDialogListener;
import com.lotosuiteplay.level.util.rackMonthPicker.listener.OnCancelMonthDialogListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


/**
 * Created by wpinango on 6/27/17.
 */

public class BalanceFragment extends Fragment {
    private ProgressBar progressBar;
    private ListView lvBalance;
    private ImageButton btnRefresh, btnHelp, btnMonPicker;
    private ArrayList<Balance> balances = new ArrayList<>();
    private String startDateBalance;
    private String endDateBalance;
    private BalanceListAdapter balanceListAdapter;
    private EditText etCashCollect, etCashDefered, etCashBloqued;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private String balanceIdTag = "balanceId1";
    private String balanceTag = "balance1";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_balance, null);
        lvBalance = (ListView) rootView.findViewById(R.id.lv_balance);
        btnRefresh = (ImageButton) rootView.findViewById(R.id.btn_refresh_balance);
        btnHelp = (ImageButton) rootView.findViewById(R.id.btn_help_balance);
        btnMonPicker = (ImageButton) rootView.findViewById(R.id.btn_mon_picker);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.sw_balance);
        progressBar = (ProgressBar) rootView.findViewById(R.id.pb_balance);
        progressBar.setVisibility(View.INVISIBLE);
        etCashCollect = (EditText) rootView.findViewById(R.id.et_cash);
        etCashCollect.setGravity(Gravity.CENTER);
        etCashBloqued = (EditText) rootView.findViewById(R.id.et_cash_bloqued);
        etCashBloqued.setGravity(Gravity.CENTER);
        etCashDefered = (EditText) rootView.findViewById(R.id.et_cash_dif);
        etCashDefered.setGravity(Gravity.CENTER);
        if ((SharedPreferenceConstants.getGeneralData(getActivity(),balanceTag) != "") && checkSavedDataMonth() && balances.isEmpty()) {
            balances.addAll(Arrays.asList(new Gson().fromJson(SharedPreferenceConstants.getGeneralData(getActivity(), balanceTag), Balance[].class)));
        }
        balanceListAdapter = new BalanceListAdapter(getActivity(), balances);
        lvBalance.setAdapter(balanceListAdapter);
        RackMonthPicker.currentMonth = Time.getCurrentMonth();
        btnMonPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnMonPicker.setEnabled(false);
                showMonthPicker();
            }
        });
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //btnRefresh.setEnabled(false);
                RackMonthPicker.currentMonth = Time.getCurrentMonth();
                setFirstAndLastDayOfMonth(Time.getCurrentMonth(), Time.getCurrentYear());
                new GetBalance().execute(Global.URL_REQUEST_BALANCE);
            }
        });
        btnHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHelp();
            }
        });
        if (balanceListAdapter.isEmpty()) {
            setFirstAndLastDayOfMonth(Time.getCurrentMonth(), Time.getCurrentYear());
            new GetBalance().execute(Global.URL_REQUEST_BALANCE);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            etCashCollect.setText(Format.getCashFormat(Integer.parseInt(Global.cash)));
            etCashBloqued.setText(Format.getCashFormat(Global.locked));
            etCashDefered.setText(Format.getCashFormat(Global.deferred));
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void refreshContent() {
        setFirstAndLastDayOfMonth(Time.getCurrentMonth(), Time.getCurrentYear());
        new GetBalance().execute(Global.URL_REQUEST_BALANCE);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    public void updateBalanceAmounts(){
        try {
            etCashCollect.setText(Format.getCashFormat(Integer.parseInt(Global.cash)));
            etCashBloqued.setText(Format.getCashFormat(Global.locked));
            etCashDefered.setText(Format.getCashFormat(Global.deferred));
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void showMonthPicker() {
        new RackMonthPicker(getActivity())
                .setPositiveButton(new DateMonthDialogListener() {
                    @Override
                    public void onDateMonth(int month, int startDate, int endDate, int year, String monthLabel) {
                        setFirstAndLastDayOfMonth((month - 1), year);
                        RackMonthPicker.currentMonth = month - 1;
                        new GetBalance().execute(Global.URL_REQUEST_BALANCE);
                        btnMonPicker.setEnabled(true);
                    }
                })
                .setNegativeButton(new OnCancelMonthDialogListener() {
                    @Override
                    public void onCancel(android.support.v7.app.AlertDialog alertDialog) {
                        alertDialog.cancel();
                        btnMonPicker.setEnabled(true);
                    }

                })
                //.setCancelable(false)
                .show();
    }

    private void setFirstAndLastDayOfMonth(int month, int year) {
        Calendar gc = new GregorianCalendar();
        gc.set(Calendar.YEAR, year);
        gc.set(Calendar.MONTH, month);
        gc.set(Calendar.DAY_OF_MONTH, 1);
        Date monthStart = gc.getTime();
        gc.add(Calendar.MONTH, 1);
        gc.add(Calendar.DAY_OF_MONTH, -1);
        Date monthEnd = gc.getTime();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yy 00:00:00");
        startDateBalance = format.format(monthStart);
        endDateBalance = format.format(monthEnd);
    }

    private void showHelp() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("RB: Recarga Bancaria");
        arrayList.add("RE: Recarga Efectivo");
        arrayList.add("SR: Saldo   Recibido");
        arrayList.add("ST: Saldo   Transferido");
        arrayList.add("CT: Cobro   Transferencia");
        arrayList.add("CE: Cobro   Efectivo");
        arrayList.add("TJ: Ticket  Jugado");
        arrayList.add("TP: Ticket  Premiado");
        arrayList.add("TA: Ticket  Anulado");
        arrayList.add("3P: Triple  Play");
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, arrayList);
        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setTitle("Leyenda");
        builder.setNegativeButton("Cerrar", null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private boolean checkSavedDataMonth() {
        try {
            ArrayList<Balance> b = new ArrayList<>();
            b.addAll(Arrays.asList(new Gson().fromJson(SharedPreferenceConstants.getGeneralData(getActivity(), balanceTag), Balance[].class)));
            int month = Integer.valueOf(b.get(0).getOperationDate().split("-")[1]);
            if (month == Time.getCurrentMonth() + 1) {
                return true;
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return false;
    }

    private class GetBalance extends AsyncTask<String, String, String> {
        private Gson gson = new Gson();
        private String hashBalance;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                btnRefresh.setEnabled(true);
                progressBar.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.getMessage();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            JsonObject dateToConsult = new JsonObject();
            dateToConsult.addProperty("startDate", startDateBalance);
            dateToConsult.addProperty("endDate", endDateBalance);
            try {
                hashBalance = SharedPreferenceConstants.getGeneralData(getActivity(),balanceIdTag);
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(Global.KEY_TOKEN, Global.token)
                        .header(Global.KEY_AGENCY_ID, Global.agencyId)
                        .header(Global.KEY_ID_USER, Global.userId)
                        .header(Global.KEY_SCHEMA,Global.KEY_MOVIL)
                        .header(Global.KEY_HASH_BALANCE, hashBalance)
                        .contentType("application/json")
                        .send(dateToConsult.toString())
                        .connectTimeout(Global.httpRequestTimeout)
                        .readTimeout(Global.httpRequestTimeout);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                Global.Toaster.get().showToast(getActivity(), "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
             }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            try {
                balances.clear();
                Response response = gson.fromJson(s, Response.class);
                if (response.error.equals("")) {
                    hashBalance = "";
                    if (!hashBalance.equals(response.getHashBalance())) {
                        SharedPreferenceConstants.saveGeneralData(getActivity(), response.getHashBalance(), balanceIdTag);
                        MyBalance mb = gson.fromJson(response.message, MyBalance.class);
                        Global.locked = mb.getLocked();
                        Global.deferred = mb.getDeferred();
                        etCashDefered.setText(Format.getCashFormat(Global.deferred));
                        etCashBloqued.setText(Format.getCashFormat(Global.locked));
                        Global.cash = String.valueOf(mb.getAmount());
                        etCashCollect.setText(Format.getCashFormat(Integer.valueOf(Global.cash)));
                        balances.clear();
                        balances.addAll(mb.getBalances());
                        if (SharedPreferenceConstants.getGeneralData(getActivity(),balanceTag) != "") {
                            balances.addAll(Arrays.asList(new Gson().fromJson(SharedPreferenceConstants.getGeneralData(getActivity(), balanceTag), Balance[].class)));
                        }
                        balanceListAdapter.notifyDataSetChanged();
                        ((MainActivity) getActivity()).setNewRechargeCash();
                        SharedPreferenceConstants.saveGeneralData(getActivity(),new Gson().toJson(balances),balanceTag);
                    }
                } else {
                    Global.Toaster.get().showToast(getActivity(), response.message, Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
