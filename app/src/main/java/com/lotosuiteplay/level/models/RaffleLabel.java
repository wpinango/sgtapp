package com.lotosuiteplay.level.models;

import android.support.annotation.NonNull;

import com.lotosuiteplay.level.common.Format;
import com.lotosuiteplay.level.common.Time;
import com.lotosuiteplay.level.Global;

import java.util.ArrayList;

public class RaffleLabel implements Comparable<RaffleLabel> {

    private int hour;
    public String game;
    private Raffle raffle;
    //public int id;
    //public boolean selected;
    //public boolean active;
    //public int gameId;
    //public String codeName;

    public RaffleLabel( String tGame,Raffle raffle, int hour){
        this.raffle = raffle;
        this.game = tGame;
        this.hour = hour;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.setHour(hour);
    }

    public boolean isSelected() {
        return raffle.isSelected();
    }

    public void setSelected(boolean selected) {
        this.raffle.setSelected(selected);
    }

    @Override
    public String toString() {
        return game + " " + Format.getRaffleHour(raffle.getHour());
    }

    @Override
    public int compareTo(@NonNull RaffleLabel o) {
        return this.getHour() > o.getHour() ? 1 : -1;
    }

    public static void setSelectedRaffle(ArrayList<RaffleLabel>raffles) {
        for (RaffleLabel raffle : raffles){
            if (raffle.isSelected()){
                for (RaffleLabel rafflesByGames : Global.rafflesByGames){
                    if (rafflesByGames.getHour() == raffle.getHour()){
                        rafflesByGames.setSelected(true);
                    } else {
                        rafflesByGames.setSelected(false);
                    }
                }
            }
        }
    }

    public static void clearRaffleSelection(ArrayList<RaffleLabel>raffleLabels) {
        for (RaffleLabel raffle : raffleLabels) {
            raffle.setSelected(false);
        }
    }

    public static boolean isRaffleSelected(ArrayList<RaffleLabel>raffleLabels){
        for (RaffleLabel raffle : raffleLabels) {
            if (raffle.isSelected()){
                return true;
            }
        }
        return false;
    }

    public static void checkRaffleTime(ArrayList<RaffleLabel>raffleLabels) {
        for (int i = raffleLabels.size() - 1; i >= 0; i--) {
            if (raffleLabels.get(i).getHour() <= Time.getCurrentTime()) {
                raffleLabels.remove(i);
            }
        }
    }

    public static RaffleLabel getSelectedRaffle(ArrayList<RaffleLabel>raffleLabels){
        for (RaffleLabel raffle : raffleLabels) {
            if (raffle.isSelected()){
                return raffle;
            }
        }
        return null;
    }

}
