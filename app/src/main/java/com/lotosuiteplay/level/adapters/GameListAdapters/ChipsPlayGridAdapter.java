package com.lotosuiteplay.level.adapters.GameListAdapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lotosuiteplay.level.Global;
import com.lotosuiteplay.level.models.Chip;
import com.lotosuiteplay.level.R;

import java.util.ArrayList;

/**
 * Created by wpinango on 9/7/17.
 */

public class ChipsPlayGridAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Chip> safariChips;
    private LayoutInflater inflater;

    public ChipsPlayGridAdapter(Context cContexto, ArrayList<Chip> safariChips) {
        this.context = cContexto;
        this.safariChips = safariChips;
    }

    @Override
    public int getCount() {
        return safariChips.size();
    }

    @Override
    public Object getItem(int position) {
        return safariChips.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View rootView, ViewGroup parent) {
        if (rootView == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rootView = inflater.inflate(R.layout.item_grid_chips, parent, false);
        }
        Chip chip = safariChips.get(position);
        TextView tvSafari = (TextView) rootView.findViewById(R.id.tv_chip_description);
        TextView tvChip = (TextView) rootView.findViewById(R.id.tv_chip);
        ImageView imageView = (ImageView) rootView.findViewById(R.id.imageView12);
        try {
            imageView.setImageResource(context.getResources().getIdentifier(chip.description.toLowerCase().trim(),"drawable",context.getPackageName()));
            if (chip.description.equals(Global.KEY_PINEAPPLE)){
                imageView.setImageResource(R.drawable.pina);
            }
            tvSafari.setText(chip.description);
            tvChip.setText(chip.chipLabel);
            if (chip.isSelected()) {
                imageView.setBackgroundColor(Color.LTGRAY);
            } else {
                imageView.setBackgroundColor(Color.WHITE);
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return rootView;
    }
}
